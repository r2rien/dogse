#!/bin/sh
## generate json from extensions dir

## common defs only once
[ -z "${D_EXT}" ] && . $(dirname $(realpath $0))/do

for proto in ${D_AVAILS} ${D_ACTIVE}; do
    e_json=${D_EXT}/list_${proto}.json
    e_json_tmp=${e_json}.tmp
    d_ext=${D_EXT}/${proto}

    echo ${e_json}
    printf "[" > ${e_json_tmp}
    for d in ${d_ext}/*; do
        n=$(basename ${d})
        fj=${d}/${F_META}
        [ ! -f ${fj} ] && continue
        jg=$(jgrep -i ${fj} -s "uuid version name" -c)
        #~ l=$(printf "${jg},\"dir\":\"${n}\"}" |sed 's/},/,/g')
        l=${jg}
        printf "${l}," >> ${e_json_tmp}
    done
    printf "]" >> ${e_json_tmp}
    sed 's/,]/]/g' -i ${e_json_tmp}
    jgrep -i ${e_json_tmp} > ${e_json}
    rm ${e_json_tmp} 2>/dev/null 
done
