# dogse

Scripts to **do** gnome shell extensions (**gse**) handling


## Why?

Digging into gnome-shell extensions and its ~~poor~~ evolving handling within gnome-desktop (beyond gnome-tweaks)  
and extensions/desktop breakages pushed me to write some scripts to ~~reduce~~ adapt the burden of their maintenance 

These scripts serves me, against a specified gnome-shell version, typically to:
- Install a batch of extensions and their known tested preferences at a fresh install
- Quickly install and activate a new extension from its url
- Watch for extensions updates
- Test extensions updates, and easily revert them if needed
- Save installed extensions preferences
- Keep a cache of gnome.org metadata online fetchs to not abuse it / quicken repeated tests
- Quickly provide patched extensions versions while waiting for official fix/update
- Easily deploy tested extensions updates
- Provide customisation, while keeping mechanics as generic as possible thanks to git branching
- Watch and adapt to gnome-shell extensions handling evolution ([lately its forced auto-update](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/1099))


## Customization

The repo consist in a generic branch `main` without any customization and the target of scripts updates.  
The second one `r2rien` is the custom one and where I keep my setups and choices.  
Thus a easy way could be to start with the custom branch to create a new one.


## Setup

These scripts uses some external commands.
See `DEPENDS` file listing them for debian environments:
 * `jq`
 * `pup`
 * `sed`
 * `curl`
 * `unzip`
 * `rsync`
 * `libxml2-utils`


## Installation

Personally, I install them via:  
~~~sh
git clone -b r2rien git@codeberg.org:r2rien/dogse.git /path/to/gse
~~~
Then as I use dogse to handle pre-packaged ones I run  
~~~sh
/path/to/gse/do_unpkg.sh
~~~
To finally run  
~~~sh
/path/to/gse/do --skip-fetch
~~~


## Usage

### prompts
Any action, except with `--assume-yes` option, will be prompted with "y|n" confirmation.  
Default action will always be "Yes", and any non empty answer will be considered as a "No",  
:warning: even if explicitely answered 'y' for example.  
Thus, only pressing <kbd>enter</kbd> will be considered as a confirmation

### first install
At first run as first install,  
default gnome local expected dir `~/.local/share/gnome/shell/extensions`  
is then symbolly linked upon It's `/path/to/gse/D_ACTIVE` sub-dir  

The contents of this `D_ACTIVE` sub-dir are only symbolic links to available extensions versions  
in another sub-dir: `/path/to/gse/D_AVAILS`

:warning: Prior to this first run, If you already had some installed extensions they will thus be moved/rsync'ed there.

See `/path/to/gse/do` for all sud-dirs definitions  
like hereafter **`I_EXT`** filename, defaulting to `index`

The symbolic links toggling game is then done after a nomenclaturated list,  
as a simple text file serving as an index:  

### index of extensions

**`/path/to/gse/I_EXT`**
> ~~~ini
> # Comment: "name" upgrade tests
> OFF name@domain.tld 1 https://extensions.gnome.org/extension/1234/gse-name/
> name@domain.tld 2 https://extensions.gnome.org/extension/1234/gse-name/
> ~~~
> | _status_ | [uuid]          | [version] | [fetch_url]          |
> | :------- | :-------------- | :-------- | :------------------- |
> | OFF      | name@domain.tld | 1         | ${G_FQDN}/id/id-name |
> | _ON_     | name@domain.tld | 2         | ${G_FQDN}/id/id-name |

### nomenclature

The nomenclature of this index, associated with definitions:  
> `/path/to/gse/do`
> ~~~ini
> [...]
> G_FQDN=extensions.gnome.org
> G_URL=https://${G_FQDN}/extension-data
> G_URI=shell-extension
> [...]
> ~~~

Addresses all the needs to handle gse:

- **[uuid]** in its metadata.json : `name@domain.tld`
- ↳ sanitized/encoded Cf ${G_FQDN} ~~> **[uri]**
- id-name in its metadata.json (but only for ~"official" extensions) : `gse-name`
- id on extensions.gnome.org : `1234`
- ↳ fetch version url ~~> `${G_FQDN}`/extension/`1234`/`gse-name`/ ==> **[fetch_url]**
- ↳ computed against specified gnome-shell version Cf `/path/to/gse/gs_version` ~~> **[version]**
- ↳ archive url ~~> `${G_URL}`/**[uri]**.v **[version]**.`${G_URI}`.zip
- ↳ extracted ~~> `/path/to/gse/D_AVAILS`/**[uri]**.v **[version]**.`${G_URI}`
- ↳ linked as ~~> `/path/to/gse/D_ACTIVE`/**[uuid]**

### scenarios

- Constraining extensions version against a declared gnome-shell version
  - Set it in `/path/to/gse/gs_version`
- Checking against changes without processing any
  - Running `/path/to/gse/do --dry-run`
- Bypassing debian package system maintained extension version
  - Getting its metadata from installed one
  - Reporting its info in a new entry in index
  - Running `/path/to/gse/do_unpkg.sh`
  - Running `/path/to/gse/do_index.sh`
- Forgetting about an extension, but keeping its patched files for reference
  - Deleting its entry in index
  - Running `/path/to/gse/do_index.sh`
- Checking for updates to already installed ones
  - Running `/path/to/gse/do`
- Testing/reverting extension versions updates
  - Prefixing with "`OFF `" to toggle one version or another
  - Running `/path/to/gse/do_index.sh`
- Install a new extension from its url without update checks of installed ones
  - Adding its entry in index with only its `http://${G_FQDN}/id/id-name` url
  - Running `/path/to/gse/do --skip-fetch`
- Batch installing of extensions and their preferences
  - Assuming extensions already listed in index
  - Assuming optional settings are present in `D_DCONF` sub-dir
  - Running `/path/to/gse/do --assume-yes`
- Saving extension preferences
  - Assuming extensions already installed and listed in index
  - Running `/path/to/gse/do --dconf-only`
- Fixing gnome-shell auto-update overruling installed version
  - Running `/path/to/gse/do --fixes-only`
- Processing only _"uuid-x"_ and _"uuid-y"_ extensions without any online fetch
  - Running `/path/to/gse/do --skip-fetch uuid-x uuid-y`


##  Options

```sh
/path/to/gse/do [OPTIONS] [EXTENSION_UUID_LIST]
```

When an optional `[EXTENSION_UUID_LIST]` (space separated) is provided,  
only the ones matching an uuid in `I_EXT` index will be processed

`-d | --dry-run`
To not process any action, but verbose about what will be done

`-f | --force-fetch`
To force online version check fetch when running in --dry-run or DBG mode

`-u | --skip-fetch`
To disable any new online fetch and re-use cached ones in `D_FETCH` subdir

`-i | --infos-only`
To only list to stdout extensions uuid with their name, url and src

`-l | --legacy-only`
To only list to stdout extensions using deprecated uri

`-o | --dconf-only`
To only save/update preferences in `D_DCONF` sub-dir

`-q | --quick-only`
To only proceed quick installs for ex. when adding a new gse extension url in index

`-x | --fixes-only`
To fix gnome-shell extension auto-update mess desynchronising `I_EXT` index

`-y | --assume-yes`
To assume yes to any prompt during process, typically in batch mode
