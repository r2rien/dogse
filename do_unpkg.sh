#!/bin/sh
# To reduce extensions list keeping only enabled ones
# /!\ after having moved the ones to keep
# /!\ to where it can be handled locally
# /!\ typically ~/.local/share/gnome-shell/

D_DPKG=/usr/share/gnome-shell/extensions
D_AVAIL=/usr/share/gnome-shell/extensions.A

[ ! -d ${D_AVAIL} ] && sudo mkdir ${D_AVAIL}
if [ -z "$(ls -A ${D_DPKG})" ]; then
    echo "nothing to disable/update"
    exit 0
fi

for f in ${D_DPKG}/*; do
    [ -L "${f}" ] && continue
    d=$(echo ${f} |awk -F / '{print $NF}')
    if [ ! -d ${D_AVAIL}/${d} ]; then
        echo "DISABLING : ${d} to ${D_AVAIL}/"
        sudo mv ${D_DPKG}/${d}/ ${D_AVAIL}/
    else
        echo "UPDATING  : ${d} to ${D_AVAIL}/"
        sudo rsync -a --delete ${D_DPKG}/${d}/ ${D_AVAIL}/${d}/
        sudo rm -Rf "${D_DPKG}/${d}/"
    fi
done

