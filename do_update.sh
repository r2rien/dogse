#!/bin/sh
## init, install, fetch and update extensions

## common defs only once
[ -z "${D_EXT}" ] && . $(dirname $(realpath $0))/do

## restricted list used with DBG=1
[ -n "${DBG}" ] && DBG="DBG "

## manually handle transitioning from OLD_v
DBG_LEGACY=

## options
while [ $# -gt 0 ]; do
    case "$1" in
        -d | --dry-run      ) DRY_RUN=1      ;;
        -f | --force-fetch  ) FORCE_FETCH=1  ;;
        -u | --skip-fetch   ) SKIP_FETCH=1   ;;
        -i | --infos-only   ) INFOS_ONLY=1   ;;
        -l | --legacy-only  ) LEGACY_ONLY=1
                            if [ -n "$2" ]; then
                                LEGACY_ONLY=$2
                                shift
                            fi               ;;
        -o | --dconf-only   ) DCONF_ONLY=1   ;;
        -q | --quick-only   ) QUICK_ONLY=1   ;;
        -x | --fixes-only   ) FIXES_ONLY=1   ;;
        -y | --assume-yes   ) ASSUME_YES=1   ;;
        *  ) [ -z "${E_FILTERS}" ] \
               && E_FILTERS="$(echo $1)" \
               || E_FILTERS="${E_FILTERS} $(echo $1)" ;;
    esac
    shift
done
tab="${tabulate}"

g_uri() { #Cf G_FQDN: 23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C
    echo $1 |tr -d '#$&+:<>=/?@[]^`{}|'; }

## init
if [ ! -L ${D_LOCAL}/${F_EXT} ]; then
    echo "${inv_on}Init${inv_off}"
    echo "${bold_on}${D_LOCAL}/${F_EXT}${bold_off}"
    ls -vlAhp --group-directories-first --color --time-style=+%d-%m-%Y·%H:%M ${D_LOCAL}/${F_EXT} 2>/dev/null
    printf "${inv_on}~~> ${D_EXT}/${D_ACTIVE}${inv_off} "
    [ -n "${ASSUME_YES}" ] && echo || read -p "${YES_no}" do_ext
    if [ -z "${do_ext}" ]; then
        INIT=1
        for d_do in ${D_ACTIVE} ${D_AVAILS} ${D_DCONF} ${D_FETCHS} ${D_ZIPS} ${D_ARCHIVES}; do
            [ ! -d ${D_EXT}/${d_do} ] && mkdir -p ${D_EXT}/${d_do}
        done
        ARCHIVED=
        for fp in ${D_LOCAL}/${F_EXT}/*; do
            e=$(basename ${fp})
            [ ! -e "${D_LOCAL}/${F_EXT}/${e}" ] && continue
            if [ -d ${D_EXT}/${D_AVAILS}/${e} ]; then
                rsync -a ${D_LOCAL}/${F_EXT}/${e}/ ${D_EXT}/${D_AVAILS}/${e}/ 2>/dev/null
            else
                rsync -a ${D_LOCAL}/${F_EXT}/${e} ${D_EXT}/${D_ARCHIVES}/ 2>/dev/null
                ARCHIVED=1
            fi
        done
        rm -Rf ${D_LOCAL}/${F_EXT}
        ln -s ${D_EXT}/${D_ACTIVE} ${D_LOCAL}/${F_EXT}
        done_ls=${D_LOCAL}/${F_EXT}
        [ -n "${ARCHIVED}" ] && done_ls="${D_EXT}/${D_ARCHIVES} ${do_ls}"
        for do_ls in ${done_ls}; do
            echo "${bold_on}${do_ls}${bold_off}"
            ls -vlAhp --group-directories-first --color --time-style=+%d-%m-%Y·%H:%M ${do_ls} 2>/dev/null
        done
    fi
fi

## extension list from index
CHANGED=
[ -z "${DBG}" ] && g_pattern="^#\|^${E_OFF}\|^DBG" || g_pattern="^${DBG}"
[ -z "${DBG}" ] && g_option='-v '                  || g_option=
for ee in $(grep ${g_option} ${g_pattern} ${EXT_LIST} |awk /./ |tr " " ","); do
    e_url=$(echo ${ee} |awk -F , '{print $NF}')
    expected_metas=$(echo ${ee} |grep -o , |wc -l)
    e_legacy=

    if [ ! "${expected_metas}" -eq 2 ]; then
        ## extension uuid from url and other meta after quick install fetch
        quick_install=true
        e=",,,${e_url}"
        e_uuid=$(echo ${e_url} |awk -F / '{print $(NF-1)}')
        v_set='?'
        e_uri=${e_uuid}

    else
        ## extension uuid and other meta from index
        [ -n "${QUICK_ONLY}" ] && continue
        quick_install=
        e=${ee}
        e_uuid=$(echo ${e} |awk -F , '{print $(NF-2)}')
        v_set=$(echo ${e} |awk -F , '{print $(NF-1)}')
        legacy_uri=$(echo ${v_set} |grep -c ^${E_LEGACY})
        if [ ${legacy_uri} -eq 0 ]; then
            e_uri=$(g_uri ${e_uuid})
            if [ "${e_uuid}" = "${DBG_LEGACY}" ]; then
                echo "${e_uuid} \n~~> e_uri\n${e_uri}"
                exit 100
            fi
        else
            e_legacy=${E_LEGACY}
            e_uri=${e_uuid}
            v_set=$(echo ${v_set} |sed "s/${E_LEGACY}//g")
            if [ "${e_uuid}" = "${DBG_LEGACY}" ]; then
                echo "${e_uuid} \n~~> e_uri\n${e_uri}"
                exit 200
            fi
        fi
    fi

    ## handle as subset
    if [ -n "${E_FILTERS}" ]; then
        in_filter=
        for f_uuid in ${E_FILTERS}; do
            if [ "${e_uuid}" = "${f_uuid}" ]; then
                in_filter=true
                break
            fi
        done
        [ -z "${in_filter}" ] && continue
    fi
    
    ## self meta
    handled=$(echo ${e_url} |grep -c ${G_FQDN})
    e_dir=${D_EXT}/${D_AVAILS}/${e_uri}.v${v_set}.${G_URI}
    e_meta=${e_dir}/${F_META}
    e_name=
    e_src=
    v_next=
    do_fetch=
    e_skip_fetch=

    label="${bold_on}${e_uuid}${bold_off} [${v_set}]"
    label_done=

    # --legacy-only
    if [ -n "${LEGACY_ONLY}" ]; then
        if [ -n "${e_legacy}" ]; then
            echo ${label}
            label_done=1
            echo "${tab} ${red_on}LEGACY${red_off} ${e_uri}"
            continue
        else
            if [ -z "${quick_install}" ]; then
                for l_uuid in $(echo ${LEGACY_ONLY} |tr "," " "); do
                    if [ "${e_uuid}" = "${l_uuid}" ]; then
                        not_continue=1
                        break
                    fi
                done
                [ -z "${not_continue}" ] && continue
            fi
        fi
    fi

    ## --infos-only
    if [ -n "${INFOS_ONLY}" ]; then
        echo ${label}
        if [ -f ${e_meta} ]; then
            infos='name uri url src'
            e_name="${inv_on}$(jq -r '.name' < ${e_meta})${inv_off}"
            [ -n "${e_legacy}" ] && e_uri="${red_on}legacy: ${red_off}${e_uri}"
            e_src=$(jq -r '.url' < ${e_meta})
        else
            infos='uri url'
        fi
        for i in ${infos}; do 
            echo "${tab} ${i}: $(echo $(eval echo "\$e_${i}"))"
        done
        continue
    fi

    ## --dconf-only
    if [ -n "${DCONF_ONLY}" ]; then
        e_gxml=$(find ${e_dir} -name *.gschema.xml)
        if [ -n "${e_gxml}" ]; then
            echo ${label}
            e_dconf=$(xmllint --xpath \
                'string(/schemalist/schema/@path)' ${e_gxml} \
                |awk -F / '{print $(NF-1)}' \
            )
            echo "${tab} ${e_dconf}"
            d_dconf=${D_EXT}/${D_DCONF}/${e_dconf}
            [ ! -d ${d_dconf} ] && mkdir -p ${d_dconf}
            dconf dump ${G_DCONF}/${e_dconf}/ > ${d_dconf}/${e_dconf}.dconf
        fi
        continue
    fi

    ## no metadata file : INSTALL | quick_install
    if [ ! -f ${e_meta} ]; then
        do_fetch="INSTALL"
        echo ${label}
        label_done=1
        v_prev="${E_LEGACY}0"
        [ -z "${quick_install}" ] && v_next=${v_set}

    ## extension meta from file
    else
        v_prev=$(jq -r '.version' < ${e_meta})

        ## gse auto-updates fixes ?
        if [ ! "${v_set}" = "${v_prev}" -a ${handled} -gt 0 ]; then
            echo ${label}
            label_done=1
            e_skip_fetch=1
            v_next=${v_set}
            printf "${tab} FIX auto-updated to [v${v_prev}] "
            if [ -n "${DRY_RUN}" ]; then
                echo
            else
                [ -n "${ASSUME_YES}" ] && echo || read -p "${YES_no}" do_fix
                if [ -z "${do_fix}" ]; then
                    rm ${D_EXT}/${D_ACTIVE}/${e_uuid}
                    if [ ! -d ${D_EXT}/${D_AVAILS}/${e_uri}.v${v_prev}.${G_URI} ]; then
                        mv ${D_EXT}/${D_AVAILS}/${e_uri}.v${v_set}.${G_URI} \
                          ${D_EXT}/${D_AVAILS}/${e_uri}.v${v_prev}.${G_URI}
                        sed -i "s/${e_uuid} ${v_set}/${e_uuid} ${v_prev}/g" ${EXT_LIST}
                    else
                        rm -Rf ${D_EXT}/${D_AVAILS}/${e_uri}.v${v_set}.${G_URI}
                    fi
                    ${DO_INDEX} --tabulate ${e_uuid}
                fi
            fi
        else
            [ -n "${FIXES_ONLY}" ] && continue
        fi
    fi

    ## print label only once
    [ -z "${label_done}" ] && echo ${label}

    ## skip unhandled
    if [ ${handled} -eq 0 ]; then
        echo "${tab} ~~ not handled"
        continue
    fi

    ## online fetch / compute versions ?
    if [ -f ${e_meta} -o -n "${quick_install}" ]; then
        d_fetch=${D_EXT}/${D_FETCHS}/${e_uuid}
        e_html=${d_fetch}/${F_HTML}
        e_versions=${d_fetch}/${F_JSON}

        # set online fetch skip on global option,
        # only if not already set in gse auto-updates fixes
        [ -z "${e_skip_fetch}" ] && e_skip_fetch=${SKIP_FETCH}

        # do online fetch ?
        prompt_fetch=
        if [ -f ${e_html} ]; then
            # already done before
            if [ -n "${e_skip_fetch}" ]; then
                if [ -n "${DBG}" -o -n "${DRY_RUN}" ]; then
                    printf "${tab} Re-use fetch "
                    if [ -n "${FORCE_FETCH}" ]; then
                        e_skip_fetch=
                        echo
                    else
                        [ -n "${ASSUME_YES}" ] && echo || read -p "${YES_no}" prompt_fetch
                    fi
                    [ -z "${prompt_fetch}" ] && e_skip_fetch=1 || e_skip_fetch=
                fi
            fi
            # for now, compute versions if not done yet
            [ ! -f ${e_versions} ] && e_do_versions=1 || e_do_versions=
        else
            # not done yet
            [ -n "${quick_install}" ] && e_skip_fetch=
            # for now, without fetch do not compute versions
            e_do_versions=
        fi

        ## online fetch
        if [ -z "${e_skip_fetch}" ]; then
            [ ! -d ${d_fetch} ] && mkdir -p ${d_fetch}
            e_do_versions=1
            echo "${tab} ${green_on}.. ${e_url}${green_off}"
            curl -A "${C_OPT}" -s ${e_url} -o ${e_html}
            e_html_changing=$(pup -l 1 '[name="csrfmiddlewaretoken"]' < ${e_html} |awk -F '"' '{print $(NF-1)}')
            sed "s/${e_html_changing}/freezed/g" -i ${e_html}
        fi

        ## compute versions only if previously fetched
        if [ -f "${e_html}" ]; then

            ## handle quick install
            if [ -n "${quick_install}" ]; then
                ## update metas with real uuid vs from quick install url
                e_uuid_tmp=${e_uuid}
                e_uuid=$(pup -l 1 "[data-uuid]" < ${e_html} \
                            |awk -F 'data-uuid="' '{print $2}' \
                            |awk -F '"' '{print $1}')
                d_fetch_tmp=${D_EXT}/${D_FETCHS}/${e_uuid_tmp}
                d_fetch=${D_EXT}/${D_FETCHS}/${e_uuid}
                e_html=${d_fetch}/${F_HTML}
                e_versions=${d_fetch}/${F_JSON}
                e_uri=$(g_uri ${e_uuid})
                do_prompt=

                ## previous temporary fetch dir update
                if [ ! "${e_uuid}" = "${e_uuid_tmp}" ]; then 
                    if [ ! -d ${d_fetch} -a -d ${d_fetch_tmp} ]; then
                        mv ${d_fetch_tmp} ${d_fetch}
                    fi
                    ## temporary index update
                    sed -i "s#^${e_url}#${e_uuid} ${v_prev} ${e_url}#g" ${EXT_LIST}
                fi
            fi

            ## compute versions
            if [ -n "${e_do_versions}" -a -f ${e_html} ]; then
                pup -l 1 "[data-uuid=\"${e_uuid}\"]" < ${e_html} \
                    |awk -F 'data-svm="' '{print $2}' \
                    |awk -F '"' '{print $1}' \
                    |sed 's/&#34;/"/g' \
                > ${e_versions}
            fi
        fi

        ## get next version except if already set in gse auto-updates fixes
        [ -z "${v_next}" ] && \
        v_next=$(jq ".\"${GS_VERSION}\".\"version\"" 2>/dev/null < ${e_versions})

        ## handle next version error
        if [ -z "${v_next}" ]; then
            echo "${tab} ${bold_on}${red_on}?? unable to compute version${red_off}${bold_off}"
            continue
        fi

        ## re-get last known version when not matching GS_VERSION
        if [ "${v_next}" = "null" ]; then
            printf "${tab} ?? gs version: \"${GS_VERSION}\" not in fetch  "

            ## deleted version upstream and not more in fetchs ?
            ## check in current avail if there is a matching GS_VERSION
            c_versions=$(jq '."shell-version" | join(" ")' ${D_EXT}/${D_ACTIVE}/${e_uuid}/${F_META} |tr -d '"')
            for gs_avail in ${c_versions}; do
                ## then if matching current GS_VERSION, keep it
                ## to prevent upgrading to a potentially incompatible version
                if [ "${gs_avail}" = "${GS_VERSION}" ]; then
                    v_keep=${v_prev}
                    break
                fi
            done
            if [ -n "${v_keep}" ]; then
                echo "~~> but in current avail  "
                v_next=${v_prev}
            else
                gs_version_last=$(jq keys_unsorted < ${e_versions} |jq '.[-1]')
                v_next=$(jq ".${gs_version_last}.\"version\"" < ${e_versions})
                echo "~~> use last: ${gs_version_last}"
            fi
        fi

        ## quick install : ante-final index update
        if [ -n "${quick_install}" ]; then
            sed -i "s#^${e_uuid} ${v_prev} ${e_url}#${e_uuid} ${v_next} ${e_url}#g" ${EXT_LIST}
            if [ -n "${DRY_RUN}" ]; then
                sed -i "s#^${e_uuid} ${v_next} ${e_url}#${e_url}#g" ${EXT_LIST}
                rm -rf ${d_fetch} ${d_fetch_tmp}
            fi
        fi
    fi

    ## update ?
    if [ -f ${e_meta} ]; then
        if [ "${v_prev}" = "${v_next}" ]; then
            echo "${tab} OK"
        else
            do_fetch="UPDATE v${v_prev} ~~>"
        fi
    fi

    ## install or update
    if [ -n "${do_fetch}" ]; then
        echo "${tab} ${do_fetch} v${v_next}"
        new_state=

        next_uri=${e_uri}.v${v_next}.${G_URI}

        ## skip if next version already disabled
        next_disabled=$(grep -c "^${E_OFF} ${e_uuid} ${v_next} ${e_url}" ${EXT_LIST})
        if [ ${next_disabled} -eq 1 ]; then
            echo "${tab} ~~> Disabled"
            continue
        fi

        ## fetch/extract next ?
        if [ ! -d ${D_EXT}/${D_AVAILS}/${next_uri} ]; then
            ## fetch archive ?
            if [ ! -f ${D_EXT}/${D_ZIPS}/${next_uri}.zip ]; then
                printf "${tab} Download "
                if [ -n "${DRY_RUN}" ]; then echo; else
                    [ -n "${ASSUME_YES}" ] && echo || read -p "${YES_no}" do_download
                    [ -n "${do_download}" ] && continue
                    curl -A "${C_OPT}" -s ${G_URL}/${next_uri}.zip -o ${D_EXT}/${D_ZIPS}/${next_uri}.zip
                    # handle quick install with legacy uri
                    if [ -n "${quick_install}" ]; then
                        has_meta=$(unzip -l ${D_EXT}/${D_ZIPS}/${next_uri}.zip 2>/dev/null |grep ${F_META})
                        if [ -z "${has_meta}" ]; then
                            e_legacy=${E_LEGACY}
                            # final index update
                            sed -i "s#^${e_uuid} ${v_next} ${e_url}#${e_uuid} ${E_LEGACY}${v_next} ${e_url}#g" ${EXT_LIST}
                            printf "${tab} Legacy: re-download "
                            [ -n "${ASSUME_YES}" ] && echo || read -p "${YES_no}" do_download
                            [ -n "${do_download}" ] && continue
                            rm ${D_EXT}/${D_ZIPS}/${next_uri}.zip
                            e_uri=${e_uuid}
                            next_uri=${e_uri}.v${v_next}.${G_URI}
                            curl -A "${C_OPT}" -s ${G_URL}/${next_uri}.zip -o ${D_EXT}/${D_ZIPS}/${next_uri}.zip
                        fi
                    fi
                fi
            fi
            ## extract archive
            printf "${tab} Extract "
            if [ -n "${DRY_RUN}" ]; then echo; else
                [ -n "${ASSUME_YES}" ] && echo || read -p "${YES_no}" do_extract
                [ -n "${do_extract}" ] && continue
                unzip -qq ${D_EXT}/${D_ZIPS}/${next_uri}.zip -d ${D_EXT}/${D_AVAILS}/${next_uri}
                new_state=1
            fi
            CHANGED="${CHANGED}\n${e_uuid}"
        fi

        ## handle next version in index
        next_enabled=$(grep -c "^${e_uuid} ${v_next} ${e_url}" ${EXT_LIST})
        if [ ${next_enabled} -eq 0 ]; then
            printf "${tab} Enable v${v_next} "
            if [ -n "${DRY_RUN}" ]; then
                echo
                new_state=1
            else
                [ -n "${ASSUME_YES}" ] && echo || read -p "${YES_no}" do_enable
                if [ -z "${do_enable}" ]; then
                    if [ ${next_disabled} -eq 0 ]; then
                        sed -i "/^${e_uuid} ${v_prev}/a ${e_uuid} ${v_next} ${e_url}" ${EXT_LIST}
                    else
                        sed -i "s/^${E_OFF} ${e_uuid} ${v_next}/${e_uuid} ${v_next}/g" ${EXT_LIST}
                    fi
                    new_state=1
                    CHANGED="${CHANGED}\n${e_uuid}"
                fi
            fi
        fi

        ## handle prev version in index
        prev_enabled=$(grep -c "^${e_uuid} ${v_prev} ${e_url}" ${EXT_LIST})
        if [ ${prev_enabled} -eq 1 ]; then
            printf "${tab} Disable v${v_prev} "
            if [ -n "${DRY_RUN}" ]; then
                echo
                new_state=1
            else
                [ -n "${ASSUME_YES}" ] && echo || read -p "${YES_no}" do_disable
                if [ -z "${do_enable}" ]; then
                    sed -i "s/^${e_uuid} ${v_prev}/${E_OFF} ${e_uuid} ${v_prev}/g" ${EXT_LIST}
                    new_state=1
                    CHANGED="${CHANGED}\n${e_uuid}"
                fi
            fi
        fi

        ## handle index change
        if [ -n "${new_state}" ]; then
            ${DO_INDEX} --tabulate ${e_uuid}
        fi

    fi

    ## handle INIT/INSTALL
    if [ -n "${INIT}" -o "${do_fetch}" = "INSTALL" ]; then

        ## load extension dconf
        e_gxml=$(find ${e_dir} -name *.gschema.xml 2>/dev/null)
        if [ -n "${e_gxml}" ]; then

            d_gxml=$(dirname ${e_gxml})
            if [ ! -f ${d_gxml}/gschemas.compiled ]; then
                echo "${tab} Compile schemas"
                glib-compile-schemas ${d_gxml}
            fi

            e_dconf=$(xmllint --xpath \
                'string(/schemalist/schema/@path)' ${e_gxml} \
                |awk -F / '{print $(NF-1)}' \
            )
            d_dconf=${D_EXT}/${D_DCONF}/${e_dconf}
            if [ -f ${d_dconf}/${e_dconf}.dconf ]; then
                printf "${tab} Load ${e_dconf}.dconf "
                if [ -n "${DRY_RUN}" ]; then
                    echo
                else
                    [ -n "${ASSUME_YES}" ] && echo || read -p "${YES_no}" do_load
                    if [ -z "${do_load}" ]; then
                        dconf load ${G_DCONF}/${e_dconf}/ < ${d_dconf}/${e_dconf}.dconf
                    fi
                fi
            fi
        fi

        ## enable extension in dconf
        g_uuids=$(\
            echo ${e_uuid} $(dconf read ${G_ECONF} |tr -d "[',]") \
            |tr " " "\n" |sort -u |sed -z "s/\n/', '/g;s/', '$/\n/"\
        )
        printf "${tab} Dconf enable "
        if [ -n "${DRY_RUN}" ]; then
            echo
        else
            [ -n "${ASSUME_YES}" ] && echo || read -p "${YES_no}" do_write
            if [ -z "${do_write}" ]; then
                dconf write ${G_ECONF} "['${g_uuids}']"
            fi
        fi
    fi

    [ -n "${DBG}" ] && exit 255
done

## handle index
if [ -n "${INIT}" ]; then
    ${DO_INDEX}
else
    ## subset index sync for info on all changes
    if [ -n "${CHANGED}" ]; then
        echo "${inv_on}changed${inv_off}"
        CHANGED=$(echo "${CHANGED}" |uniq |tr '\n' ' ')
        ${DO_INDEX} ${CHANGED}
    fi
fi
