#!/bin/sh
## enable/disable extension versions via symbolic links
## typically: uuid@url ${D_ACTIVE}/uuid@url -> ../${D_AVAILS}/uuid-dirname.vX

## common defs only once
[ -z "${D_EXT}" ] && . $(dirname $(realpath $0))/do

while [ $# -gt 0 ]; do
    case "$1" in
        -t | --tabulate  ) tab="${tabulate} " ;;
        *  ) [ -z "${E_FILTERS}" ] && E_FILTERS=$(echo $1) || E_FILTERS="${E_FILTERS} $(echo $1)" ;;
    esac
    shift
done

E_REDO=
do_set (){
e_redo=$1
for f in $(find ${D_EXT}/${D_AVAILS}/ -name ${F_META} |sort); do
    #~ e_name=$(jq -r '.name' < ${f})
    #~ v_json=v$(jq -r '.version' < ${f})
    e_uuid=$(jq -r '.uuid' < ${f})
    if [ -n "${E_FILTERS}" ]; then
        in_filter=
        for f_uuid in ${E_FILTERS}; do
            if [ "${e_uuid}" = "${f_uuid}" ]; then
                in_filter=true
                break
            fi
        done
        [ -z "${in_filter}" ] && continue
    fi
    [ -n "${e_redo}" -a "${e_redo}" != "${e_uuid}" ] && continue
    e_lnk=$(basename $(dirname ${f}))
    v_lnk=$(echo ${e_lnk} |awk -F . '{print $(NF-1)}')
    v_set=v$(grep ^${e_uuid} ${EXT_LIST} |awk '{print $2}' |sed "s/${E_LEGACY}//g")

    if [ ! -e "${D_EXT}/${D_ACTIVE}/${e_uuid}" ]; then
        if [ -n "${v_set}" -a "${v_lnk}" = "${v_set}" ]; then
            ln -sf ../${D_AVAILS}/${e_lnk} ${D_EXT}/${D_ACTIVE}/${e_uuid}
            status="ENABLED"
        else
            status="SKIPPED"
        fi
    else
        if [ ! "${v_lnk}" = "${v_set}" ]; then
            v_cur=$(basename $(readlink ${D_EXT}/${D_ACTIVE}/${e_uuid}) |awk -F . '{print $(NF-1)}')
            if [ ! "${v_cur}" = "${v_set}" ]; then
                rm ${D_EXT}/${D_ACTIVE}/${e_uuid}
                E_REDO="${e_uuid} ${E_REDO}"
                status="CHANGED"
            else
                status="SKEEPED"
            fi
        else
            status="-------"
        fi
    fi
    echo "${tab}${status} : ${v_lnk} : ${e_uuid}"
done
}

do_set
if [ -n "${E_REDO}" ]; then
    for e in ${E_REDO}; do
        echo "${tab}REDOING : ${e}"
        do_set ${e}
    done
fi
