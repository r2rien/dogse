#!/bin/sh
## common defs dealing with extensions
D_EXT=$(dirname $(realpath $0))

DO_INDEX=${D_EXT}/do_index.sh
DO_LISTS=${D_EXT}/do_lists.sh
DO_UNPKG=${D_EXT}/do_unpkg.sh
DO_UPDATE=${D_EXT}/do_update.sh
if [ $(basename $0) = "do" ]; then ${DO_UPDATE} "$@"; exit; fi

D_ACTIVE=active
D_AVAILS=avails
D_DCONF=dconfs
D_FETCHS=fetchs
D_ZIPS=zips
D_ARCHIVES=${D_AVAILS}/archives

I_EXT=index
EXT_LIST=${D_EXT}/${I_EXT}
E_OFF="OFF"
E_LEGACY="OLD_"
D_LOCAL=~/.local/share/gnome-shell
F_EXT=extensions
F_META=metadata.json
F_HTML=index.html
F_JSON=versions.json

#~ GS_VERSION=$(dpkg-query -W --showformat='${Version}\n' gnome-shell |awk -F . '{print $1"."$2}')
GS_VERSION=$(cat ${D_EXT}/gs_version)
G_DCONF=/org/gnome/shell/extensions
G_ECONF=/org/gnome/shell/enabled-extensions
G_FQDN=extensions.gnome.org
G_URL=https://${G_FQDN}/extension-data
G_URI=shell-extension
C_OPT="Mozilla/5.0 (X11; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0"

bold_on=$(tput bold)
bold_off=$(tput sgr0)
green_on=$(tput setaf 2)
green_off=$(tput sgr0)
red_on=$(tput setaf 1)
red_off=$(tput sgr0)
inv_on=$(tput smso)
inv_off=$(tput sgr0)
YES_no="[${inv_on} y ${inv_off}|n] ? "
tabulate="   "
